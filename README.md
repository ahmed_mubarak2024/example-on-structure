# **This repo is an example of MVI with testing examples and Structure ideas**

#

In your everyday life as an Android developer you are tasked with creating a new project and structure it as a Developer. I saw very skilled developers using new components but not in the structure causing the project to become not fixable to update or easily manage to add small changes.

# So ……

# **Our target in this example is to fix this**

1.
# Implement MVI structure {data source , Repositories , Viewmodel }
2.
# Add base structure to each component to collect common logic in this component or even let be empty as is will be useful in the future
3.
# Create router class to help us navigate throw the app
4.
# Handle bad Network and auto retry
5.
# Using koin to help us implement testing
6.
# Do a little unit test and show some tricks in it
7.
# Do some instrumental test

1.
# **MVI**

# MVI is improved version of MVVM for those who don&#39;t know MVVM go to this
[
# link
](https://developer.android.com/jetpack/guide)
# mvvm was great and had improved
[
# MVP
](https://www.raywenderlich.com/7026-getting-started-with-mvp-model-view-presenter-on-android#toc-anchor-010)
# alot and was easier and simpler to implement but it had its draw back for screens with a lot of request so it was updated to MVI

# **What is MVI ?**

# _MVI_

# stands for

# _Model-View-Intent_

# . MVI is one of the newest architecture patterns for Android, inspired by the unidirectional and cyclical nature of the

# _Cycle.js_

# framework.

# Check out this link for extra info for
[
# MVI
](https://www.raywenderlich.com/817602-mvi-architecture-for-android-tutorial-getting-started)
# as i will not deeply discuss it. I will only discuss my implementation.

# In the project you will find the three components of MVVM data source , repo , viewmodel

# In each you will find a base for them. In this base you will find
[
# PublishSubject
](http://reactivex.io/RxJava/javadoc/io/reactivex/subjects/PublishSubject.html)
# that is used to relay the data from each component to the next from datasource to repo to viewmodel to finally the view.

# The main difference here we don&#39;t send the data as it is we send DataEvent\&lt;T\&gt; that have in it

1.
# Identifier to differ each request from other
2.
# Event and it can be {Success , error, Started ,Ended} the normal states of a request
3.
# Data if we were in a Success state it would have the data
4.
# Error if we were in error state it will hold the error

# **In the dataSource**

# or PeopleDataSourceApi you will find me subscribing to the retrofit api and mapping the result to Success DataEvent in the dataSource publishSubject.

# **In the repo**

# or PersonsRepo

# You will find me subscribing to the publishSubject from the DataSource using the repo publishSubject and only have void methods that do the call in the dataSource.

# **In the Viewmodel**

# or PersonListingViewModel

# I only subscribe to the Repo using the viewmodel publishSubject and have void method to do the call

# **In the Fragment**

# i subscribe to the viewmodel publishSubject and handle the events to update my UI

# to be continued …..

# PS when running the Instrumental test you might have issue with the animation that cause the test to fall so i recommend disable it form the developer option /  drawing {window animation scale , transition animation scale , animator duration scale }