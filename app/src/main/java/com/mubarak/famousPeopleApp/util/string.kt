package com.mubarak.famousPeopleApp.util

import com.google.gson.Gson

fun Any.toGsonString(): String {
    return Gson().toJson(this)
}

inline fun <reified T> String.fromJson(): T? {
    return Gson().fromJson<T>(this, T::class.java)
}

inline fun String.getImagePath() =
    "${Constants.IMAGES_BASE}${this}"