package com.mubarak.famousPeopleApp.util

import android.content.Context
import android.content.Intent
import com.mubarak.famousPeopleApp.model.Person
import com.mubarak.famousPeopleApp.model.Profile
import com.mubarak.famousPeopleApp.views.activites.PersonDetailActivity
import com.mubarak.famousPeopleApp.views.fragments.personDetail.ImageFragment
import com.mubarak.famousPeopleApp.views.parents.BaseActivity

object Router {

    /**
     * @param context should be extend [BaseActivity]
     * @param person the object used to start init the UI in the person the detail
     * */
    fun goToPersonDetail(context: Context, person: Person) {
        val intent = Intent(context, PersonDetailActivity::class.java)

        intent.putExtra(PersonDetailActivity.KEY_PERSON, person.toGsonString())
        context.startActivity(intent)
    }

    /**
     * @param mContext should be extend [BaseActivity]
     * @param profile the object used to start init the UI in Image FullScreen
     * */
    fun goToImageFullScreen(mContext: Context, profile: Profile) {
        (mContext as BaseActivity).addFragment(ImageFragment.newInstance(profile), true)
    }
}