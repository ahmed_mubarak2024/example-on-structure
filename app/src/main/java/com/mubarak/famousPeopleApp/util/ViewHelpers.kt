package com.mubarak.famousPeopleApp.util

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

fun View.layoutInflater() = LayoutInflater.from(this.context)
fun View.setVisibilityState(state: Int) {
    (tag as? PublishSubject<Int>)?.onNext(state) ?: kotlin.run {
        Log.e(TAG, "you can't set state before calling add To bag")
    }
}

val Any.TAG get() = this::class.simpleName
fun View.addToBag(bag: CompositeDisposable) {
    (((tag as? PublishSubject<Int>) ?: kotlin.run {
        PublishSubject.create<Int>()
            .apply { tag = this }
    })).throttleLatest(700, TimeUnit.MILLISECONDS)
        .observeOnly().subscribe { visibility = it }?.add(bag = bag)
}