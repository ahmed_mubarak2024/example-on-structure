package com.mubarak.famousPeopleApp.util

import com.mubarak.famousPeopleApp.koin.SchedulerProvider
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import org.koin.java.KoinJavaComponent


fun Disposable.add(bag: CompositeDisposable) =
        bag.add(this)

fun <T> Single<T>.subscribeOnly() =
        this.subscribeOn(KoinJavaComponent.get(SchedulerProvider::class.java).io())

fun <T> Single<T>.observeOnly() =
        this.observeOn(KoinJavaComponent.get(SchedulerProvider::class.java).ui())

fun <T> PublishSubject<T>.observeOnly() =
        this.observeOn(KoinJavaComponent.get(SchedulerProvider::class.java).ui())

fun <T> Observable<T>.subscribeOnly() =
        this.subscribeOn(KoinJavaComponent.get(SchedulerProvider::class.java).io())


fun <T> Single<T>.load() =
        this.subscribeOn(KoinJavaComponent.get(SchedulerProvider::class.java).io())
                .observeOn(KoinJavaComponent.get(SchedulerProvider::class.java).ui())

fun <T> Observable<T>.load() =
        this.subscribeOn(KoinJavaComponent.get(SchedulerProvider::class.java).io())
                .observeOn(KoinJavaComponent.get(SchedulerProvider::class.java).ui())

fun <T> Observable<T>.observeOnly() =
        this.observeOn(KoinJavaComponent.get(SchedulerProvider::class.java).ui())

fun <T> Flowable<T>.observeOnly() =
        this.observeOn(KoinJavaComponent.get(SchedulerProvider::class.java).ui())


fun <T> Maybe<T>.load() =
        this.subscribeOn(KoinJavaComponent.get(SchedulerProvider::class.java).io())
                .observeOn(KoinJavaComponent.get(SchedulerProvider::class.java).ui())


/**
 * Use SchedulerProvider configuration for Single
 */
fun <T> Single<T>.with(
        schedulerProvider: SchedulerProvider = KoinJavaComponent.get(
                SchedulerProvider::class.java
        )
): Single<T> = subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui())


