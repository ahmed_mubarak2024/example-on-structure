package com.mubarak.famousPeopleApp.util

object Constants {

    const val IS_TESTING = "IS_TESTING"
    const val API_KEY = "6675b80b92722b89ee281264add1a4e6"

    //calls identifiers
    const val GET_POPULAR_ID = 1
    const val GET_PERSON_DETAIL = 2
    const val GET_PERSON_IMAGES = 3

    ////
    //urls
    const val IMAGES_BASE = "http://image.tmdb.org/t/p/w185"
    const val API_BASE_URL = "https://api.themoviedb.org/3/"
    const val PERSON_POPULAR = "person/popular"
    const val PERSON_DETAIL = "person/{person_id}"
    const val PERSON_IMAGES = "person/{person_id}/images"

    ///
}