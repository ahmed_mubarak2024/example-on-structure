package com.mubarak.famousPeopleApp.util

import com.bumptech.glide.request.RequestOptions
import com.mubarak.famousPeopleApp.R

/**
 * used it to add the images placeholders to all the glide requests
 * */
fun getGlideBaseOptions() = RequestOptions().placeholder(R.drawable.avatar_placeholder_generic)
    .error(R.drawable.avatar_placeholder_generic)