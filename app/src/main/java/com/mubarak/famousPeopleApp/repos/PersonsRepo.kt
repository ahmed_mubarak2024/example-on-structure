package com.mubarak.famousPeopleApp.repos

import com.mubarak.famousPeopleApp.dataSources.PeopleDataSourceApi
import com.mubarak.famousPeopleApp.model.DataEvent
import io.reactivex.disposables.CompositeDisposable
import org.koin.core.inject
import org.koin.core.parameter.parametersOf

/**
 * used to add the all the person repos
 * */
class PersonsRepo(bag: CompositeDisposable) : BaseRepo(bag) {
    private val peopleDataSourceApi: PeopleDataSourceApi by inject { parametersOf(bag) }


    init {
        initRequestChain()
    }

    /**
     * in the method we init the connection between the dataSource and the Repo
     * the doOnNext  used to handle removing identifiers the blockSet
     * */
    override fun initRequestChain() {
        peopleDataSourceApi.dataPublisher
            .doOnNext {
                if (it.event == DataEvent.ENDED) {
                    blockSet.remove(it.identifier)
                }
            }
            .subscribe(dataPublisher)
    }

    fun loadPage(index: Int, callId: Int) {
        if (callId == PeopleDataSourceApi.GET_POPULAR_ID && !blockSet.contains(callId))
            peopleDataSourceApi.loadPopular(index)
        blockSet.add(callId)
    }

    fun loadPersonDetail(personId: Int) {
        if (!blockSet.contains(PeopleDataSourceApi.GET_PERSON_DETAIL))
            peopleDataSourceApi.loadPersonDetail(personId)
        blockSet.add(PeopleDataSourceApi.GET_PERSON_DETAIL)
    }

    fun loadPersonImages(personId: Int) {
        if (!blockSet.contains(PeopleDataSourceApi.GET_PERSON_IMAGES))
            peopleDataSourceApi.loadPersonImages(personId)
        blockSet.add(PeopleDataSourceApi.GET_PERSON_IMAGES)
    }


}