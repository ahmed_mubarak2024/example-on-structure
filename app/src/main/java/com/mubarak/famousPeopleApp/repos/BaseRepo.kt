package com.mubarak.famousPeopleApp.repos

import com.mubarak.famousPeopleApp.model.DataEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import org.koin.core.KoinComponent

/**
 *@param bag from the view used to add the repo [bag] in it
 * @property bag separated from the view bag to be able to clear it any time
 * @property blockSet used to stop redundent request that are called again before the
 * first call complete
 * @property dataPublisher used to send the data to the viewmodel
 * */
abstract class BaseRepo(bag: CompositeDisposable) : KoinComponent {
    protected val blockSet: MutableSet<Int> = mutableSetOf()

    /**
     * handle clearing the request chain and clearing the blockSet to enable any request
     * to come through again
     * */
    fun restartRequestChain() {
        bag.clear()
        blockSet.clear()
    }

    /**
     * used to connect the [dataPublisher] to the [com.mubarak.famousPeopleApp.dataSources.BaseDataSource]s
     * * */
    abstract fun initRequestChain()
    protected val bag: CompositeDisposable = CompositeDisposable()
    val dataPublisher: PublishSubject<DataEvent<*>> by
    lazy { PublishSubject.create<DataEvent<*>>() }

    init {
        bag.add(this.bag)
    }
}