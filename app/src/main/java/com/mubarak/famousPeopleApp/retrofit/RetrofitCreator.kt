@file:Suppress("PrivatePropertyName")

package com.mubarak.famousPeopleApp.retrofit


import android.util.Log
import com.mubarak.famousPeopleApp.retrofit.apis.PeopleApi
import com.mubarak.famousPeopleApp.util.Constants
import okhttp3.OkHttpClient
import org.koin.core.KoinComponent
import org.koin.core.inject
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


/**
 * @author Ahmed Mubarak
 * this is used for creating retrofit
 */
class RetrofitCreator private constructor() : RetroFItTester, KoinComponent {

    companion object {
        private var instance: RetrofitCreator = RetrofitCreator()

        fun getInstance(): RetrofitCreator {
            return instance
        }

    }

    val okHttpClient by inject<OkHttpClient>()


    private val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(Constants.API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory((RxJava2CallAdapterFactory.create()))
            .client(okHttpClient)

            .build()
    }
    private val TAG = RetrofitCreator::class.java.simpleName

/**
 * used the Interceptor to add the api_key to all urls
 * */
    override fun getAuthorizedOkHttpClient(): OkHttpClient.Builder {
        return OkHttpClient.Builder()
            .addInterceptor { chain ->

                var request = chain.request()

                val builder = request.newBuilder()

                request = builder.build()
                val urlBuilder = request.url.newBuilder()
                urlBuilder.addQueryParameter("api_key", Constants.API_KEY)
                builder.url(urlBuilder.build())
                request = builder.build()
                Log.d(TAG, "${request.url}")
                val response = chain.proceed(request)
                Log.d(TAG, "${request.url}  ${request.method} ${response.code}")
                response


            }

    }


    override fun getPopularPeopleApi(): PeopleApi {
        return retrofit.create(PeopleApi::class.java)
    }


}
