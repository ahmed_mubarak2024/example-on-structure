package com.mubarak.famousPeopleApp.retrofit

import com.mubarak.famousPeopleApp.retrofit.apis.PeopleApi
import okhttp3.OkHttpClient


interface RetroFItTester {
    fun getPopularPeopleApi(): PeopleApi
    fun getAuthorizedOkHttpClient(): OkHttpClient.Builder
}