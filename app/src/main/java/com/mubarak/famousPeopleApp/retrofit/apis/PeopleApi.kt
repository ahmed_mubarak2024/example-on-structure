package com.mubarak.famousPeopleApp.retrofit.apis

import com.mubarak.famousPeopleApp.model.ImagesResponse
import com.mubarak.famousPeopleApp.model.Person
import com.mubarak.famousPeopleApp.model.Result
import com.mubarak.famousPeopleApp.util.Constants
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PeopleApi {
    @GET(Constants.PERSON_POPULAR)
    fun getPopular(@Query("page") page: Int): Single<Result<Person>>

    @GET(Constants.PERSON_DETAIL)
    fun getPersonDetail(@Path("person_id") personId: Int): Single<Person>

    @GET(Constants.PERSON_IMAGES)
    fun getPersonImages(@Path("person_id") personId: Int): Single<ImagesResponse>
}