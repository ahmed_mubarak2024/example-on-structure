package com.mubarak.famousPeopleApp.model

import com.google.gson.annotations.SerializedName

/**
 * or comes in list [com.mubarak.famousPeopleApp.util.Constants.PERSON_POPULAR]
 * in the form [Result] and [T] is [Person]
 * */
class Result<T>(
    val page: Int, @SerializedName("total_results") val totalResult: Int,
    @SerializedName("total_pages") val totalPages: Int, val results: List<T>
)