package com.mubarak.famousPeopleApp.model


import com.google.gson.annotations.SerializedName

/**
 * the response from [com.mubarak.famousPeopleApp.util.Constants.PERSON_DETAIL]
 * or comes in list [com.mubarak.famousPeopleApp.util.Constants.PERSON_POPULAR]
 * */
data class Person(
    @SerializedName("adult")
    var adult: Boolean? = null,
    @SerializedName("gender")
    var gender: Int? = null,
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("known_for_department")
    var knownForDepartment: String? = null,
    @SerializedName("name")
    var name: String? = null,
    val biography: String? = null,
    val birthday: String? = null,
    @SerializedName("popularity")
    var popularity: Double? = null,
    @SerializedName("profile_path")
    var profilePath: String? = null,
    var primaryKey: Int = 0,
    var pageNumber: Int = 0,

    )