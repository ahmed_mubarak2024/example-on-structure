package com.mubarak.famousPeopleApp.model

/**
 * used in the [com.mubarak.famousPeopleApp.views.adapter.parent.BaseAdapter] and
 * [com.mubarak.famousPeopleApp.views.adapter.parent.BaseViewHolder] to send click events and
 * other events
 * @param id to identify the event
 * @param data used the data if it is needed
 * */
class ClickEvents<T>(val id: Int, val data: T?) {
    companion object {
        const val GO_TO_IMAGE_FULL_SCREEN = 3
        const val GO_TO_PERSON_DETAIL = 2
        const val LOAD_NEXT_PAGE = 1
    }
}