package com.mubarak.famousPeopleApp.model


/**
 * used to send all the event from the dataSource to the views
 * @param identifier used to identify the requests
 * @param event takes one of the [SUCCESS] [STARTED] [ERROR] [ENDED] to represent the State
 * @param data the data form the call only [org.jetbrains.annotations.NotNull] if it is [SUCCESS]
 * @param error is only [org.jetbrains.annotations.NotNull] if it is [ERROR]
 * */
class DataEvent<T>(
    val identifier: Int, val event: Int, val data: T? = null,
    val error: Throwable? = null
) {
    companion object {
        const val SUCCESS = 1
        const val ERROR = 2
        const val STARTED = 3
        const val ENDED = 4
        fun onError(identifier: Int, throwable: Throwable) =
            DataEvent(identifier, ERROR, null, throwable)

        fun <T> onSuccess(identifier: Int, data: T) =
            DataEvent(identifier, SUCCESS, data)

        fun onStart(identifier: Int) =
            DataEvent<Any>(identifier, STARTED)

        fun onEnd(identifier: Int) =
            DataEvent<Any>(identifier, ENDED)
    }
}