package com.mubarak.famousPeopleApp.model


import com.google.gson.annotations.SerializedName

/**
 * the response from [com.mubarak.famousPeopleApp.util.Constants.PERSON_IMAGES]
 * */
data class ImagesResponse(
    @SerializedName("id")
    val id: Int?,
    @SerializedName("profiles")
    val profiles: List<Profile>?
)