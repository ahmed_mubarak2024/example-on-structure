package com.mubarak.famousPeopleApp.views.adapter.viewholder

import android.view.View
import com.bumptech.glide.Glide
import com.mubarak.famousPeopleApp.model.ClickEvents
import com.mubarak.famousPeopleApp.model.Person
import com.mubarak.famousPeopleApp.util.getGlideBaseOptions
import com.mubarak.famousPeopleApp.util.getImagePath
import com.mubarak.famousPeopleApp.views.adapter.parent.BaseViewHolder
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.cell_user.view.*

class PersonViewHolder(view: View, bag: CompositeDisposable) : BaseViewHolder<Person>(bag, view) {

    init {
        itemView.iv_people_imge.setOnClickListener {
            clicks.onNext(ClickEvents(ClickEvents.GO_TO_PERSON_DETAIL, data))
        }
    }

    override fun init(person: Person) {
        super.init(person)
        Glide.with(itemView)
            .load(person.profilePath?.getImagePath())
            .apply(
                getGlideBaseOptions()
                    .centerCrop()
            )
            .into(itemView.iv_people_imge)
        itemView.tv_work_type.text = person.knownForDepartment
        itemView.tv_people_name.text = person.name
        // ViewCompat.setTransitionName(itemView.iv_people_imge, person.id?.toString())

    }
}