package com.mubarak.famousPeopleApp.views.activites

import android.os.Bundle
import com.mubarak.famousPeopleApp.R
import com.mubarak.famousPeopleApp.views.fragments.personListing.PersonListingFragment
import com.mubarak.famousPeopleApp.views.parents.BaseActivity
import com.mubarak.famousPeopleApp.views.parents.BaseFragment

/**
 * Home screen and hold the Listing View
 * */
class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        addFragment(PersonListingFragment.newInstance(PersonListingFragment.TYPE_POPULAR))
    }

    override fun addFragment(newInstance: BaseFragment, addToBackStack: Boolean) {
        supportFragmentManager.beginTransaction()
            .add(R.id.container, newInstance)
            .commitAllowingStateLoss()
    }
}