package com.mubarak.famousPeopleApp.views.parents

import android.os.Bundle
import android.view.View
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.mubarak.famousPeopleApp.model.ClickEvents
import com.mubarak.famousPeopleApp.util.add
import com.mubarak.famousPeopleApp.util.subscribeOnly
import com.mubarak.famousPeopleApp.views.adapter.parent.BaseAdapter

abstract class BasePagingFragment<T> : BaseFragment() {
    private val listOfData: MutableList<T> = mutableListOf()
    protected abstract val identifier: Int
    protected abstract val adapter: BaseAdapter<T>
    protected var pageSize = 20
    protected var pagNum = 1
    private var requestInProgress = false
    private var lastPage = false
    open val refreshLayout: SwipeRefreshLayout? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter.clicks
            .subscribeOnly()
            .filter { !checkErrorState(identifier) }
            .subscribe {
                when (it.id) {
                    ClickEvents.LOAD_NEXT_PAGE -> {
                        if (!requestInProgress && !lastPage) {
                            requestInProgress = true
                            loadData()
                        }
                    }
                }
            }.add(bag)
        refreshLayout?.setOnRefreshListener {
            refreshLoading()
            loadData()
        }
    }

    override fun <G> onSuccess(data: G, id: Int) {
        super.onSuccess(data, id)
        if (id == identifier) {
            handlePagingRequest(data as List<T>)
            if (data.size < pageSize)
                lastPage = true
        }
    }

    override fun onComplete(id: Int) {
        super.onComplete(id)
        if (id == identifier) {
            requestInProgress = false
            refreshLayout?.isRefreshing = false
        }
    }

    private fun handlePagingRequest(list: List<T>) {
        listOfData.addAll(list)
        adapter.updateDataList(listOfData)
        pagNum++
    }

    abstract fun loadData()
    abstract fun stopRequest()
    fun refreshLoading() {
        listOfData.clear()
        adapter.updateDataList(listOfData)
        pagNum = 1
        lastPage = false
        if (requestInProgress)
            stopRequest()
        requestInProgress = false
    }
}