package com.mubarak.famousPeopleApp.views.adapter.viewholder

import android.view.View
import com.bumptech.glide.Glide
import com.mubarak.famousPeopleApp.model.ClickEvents
import com.mubarak.famousPeopleApp.model.Profile
import com.mubarak.famousPeopleApp.util.getGlideBaseOptions
import com.mubarak.famousPeopleApp.util.getImagePath
import com.mubarak.famousPeopleApp.views.adapter.parent.BaseViewHolder
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.cell_image_person.view.*

class PersonImageViewHolder(bag: CompositeDisposable, view: View) :
    BaseViewHolder<Profile>(bag, view) {
    init {
        itemView.iv_person_image_extra.setOnClickListener {
            clicks.onNext(ClickEvents(ClickEvents.GO_TO_IMAGE_FULL_SCREEN, data))
        }
    }

    override fun init(data: Profile) {
        super.init(data)
        Glide.with(itemView)
            .load(data.filePath?.getImagePath())
            .apply(getGlideBaseOptions())
            .into(itemView.iv_person_image_extra)
    }
}