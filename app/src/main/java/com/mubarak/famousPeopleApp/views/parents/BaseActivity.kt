@file:Suppress("KotlinDeprecation")

package com.mubarak.famousPeopleApp.views.parents

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.subjects.PublishSubject

/**
 * hold the network State Listener and some common logic
 * @property networkChangeReceiver the broadCast Receiver for network state
 * @property networkListener publish that we are connected or not
 * */
abstract class BaseActivity : AppCompatActivity() {
    private val networkChangeReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Log.d("app", "Network connectivity change")
            networkListener.onNext(checkNetworkConnectivity())
        }
    }
    val networkListener by lazy {
        PublishSubject.create<Boolean>()
    }

    /**
     * register the broadcast receiver
     * */
    override fun onStart() {
        super.onStart()
        val intentFilter = IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")
        registerReceiver(networkChangeReceiver, intentFilter)
    }

    /**
     * unRegister the broadcast receiver
     * */
    override fun onStop() {
        super.onStop()
        unregisterReceiver(networkChangeReceiver)
    }

    abstract fun addFragment(newInstance: BaseFragment, addToBackStack: Boolean = false)

    /**
     * @return weather we are connected to network or not
     * */
    fun checkNetworkConnectivity(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected

    }
}