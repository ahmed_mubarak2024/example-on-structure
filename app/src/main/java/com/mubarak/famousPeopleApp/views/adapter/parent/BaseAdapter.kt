package com.mubarak.famousPeopleApp.views.adapter.parent

import androidx.recyclerview.widget.RecyclerView
import com.mubarak.famousPeopleApp.model.ClickEvents
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

abstract class BaseAdapter<T>(bag: CompositeDisposable) :
    RecyclerView.Adapter<BaseViewHolder<T>>() {
    val bag: CompositeDisposable = CompositeDisposable()
    val clicks: PublishSubject<ClickEvents<T>> by lazy {
        PublishSubject.create<ClickEvents<T>>()
    }

    init {
        this.bag.add(bag)
    }

    protected val dataList: MutableList<T> = mutableListOf()

    /**
     * send [ClickEvents.LOAD_NEXT_PAGE] when we are have less than 20 item to show
     * */
    override fun onBindViewHolder(holder: BaseViewHolder<T>, position: Int) {
        getItem(position)?.let {
            holder.init(it)
        }
        if (position + 20 > itemCount)
            clicks.onNext(ClickEvents(ClickEvents.LOAD_NEXT_PAGE, dataList.last()))
    }

    protected fun getItem(position: Int) = dataList[position]
    override fun getItemCount(): Int {
        return dataList.size
    }

    /**
     * @param list the data to set on the screen
     * */
    open fun updateDataList(list: List<T>) {
        dataList.clear()
        dataList.addAll(list)
        notifyDataSetChanged()
    }

    fun BaseViewHolder<T>.addToListener(): BaseViewHolder<T> {
        clicks.subscribe(this@BaseAdapter.clicks)
        return this
    }
}