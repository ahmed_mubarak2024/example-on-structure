package com.mubarak.famousPeopleApp.views.adapter.parent

import android.view.View
import androidx.annotation.CallSuper
import androidx.recyclerview.widget.RecyclerView
import com.mubarak.famousPeopleApp.model.ClickEvents
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

abstract class BaseViewHolder<T>(bag: CompositeDisposable, view: View) :
    RecyclerView.ViewHolder(view) {
    var data: T? = null
    val bag: CompositeDisposable = CompositeDisposable()
    val clicks: PublishSubject<ClickEvents<T>> by lazy {
        PublishSubject.create<ClickEvents<T>>()
    }

    init {
        bag.add(this.bag)
    }

    @CallSuper
    open fun init(data: T) {
        this.data = data
    }
}