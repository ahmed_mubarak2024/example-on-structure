package com.mubarak.famousPeopleApp.views.fragments.personDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.mubarak.famousPeopleApp.R
import com.mubarak.famousPeopleApp.model.ClickEvents
import com.mubarak.famousPeopleApp.model.ImagesResponse
import com.mubarak.famousPeopleApp.model.Person
import com.mubarak.famousPeopleApp.util.*
import com.mubarak.famousPeopleApp.viewmodel.PersonDetailViewModel
import com.mubarak.famousPeopleApp.views.activites.PersonDetailActivity
import com.mubarak.famousPeopleApp.views.adapter.PersonImagesAdapter
import com.mubarak.famousPeopleApp.views.parents.BaseFragment
import kotlinx.android.synthetic.main.person_detail_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


class PersonDetailFragment : BaseFragment() {

    companion object {
        fun newInstance(extras: Bundle?) = PersonDetailFragment().apply {
            arguments = extras
        }
    }

    private val viewModel: PersonDetailViewModel by viewModel { parametersOf(bag) }
    private var person: Person? = null
    private val adapter by lazy {
        PersonImagesAdapter(bag)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.person_detail_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        person = arguments?.getString(PersonDetailActivity.KEY_PERSON)?.fromJson()
        person?.let { setupUi(it) }
        viewModel.addListener()
        progressBar.addToBag(bag)
        rv_images.adapter = adapter
        rv_images.layoutManager = GridLayoutManager(mContext, 2)
        ViewCompat.setTransitionName(iv_person_image, "person detail image")
        adapter.clicks
            .observeOnly()
            .subscribe {
                val data = it.data
                val context = mContext
                if (data != null && context != null)
                    if (it.id == ClickEvents.GO_TO_IMAGE_FULL_SCREEN)
                        Router.goToImageFullScreen(context, data)
            }.add(bag)
        loadData()
    }

    private fun loadData() {
        if (isNetworkAvailable(true, Constants.GET_PERSON_DETAIL, Constants.GET_PERSON_IMAGES))
            person?.id?.let {
                viewModel.loadPersonDetail(it)
                viewModel.loadPersonImages(it)
            }
    }

    private fun setupUi(person: Person) {
        mContext?.let {
            Glide.with(it)
                .load(person.profilePath?.getImagePath())
                .apply {
                    getGlideBaseOptions()
                        .centerCrop()
                }
                .into(iv_person_image)
            tv_name.text = person.name
            tv_brithday.text = person.birthday
            tv_bio.text = person.biography
        }
    }

    override fun <T> onSuccess(data: T, id: Int) {
        super.onSuccess(data, id)
        if (id == Constants.GET_PERSON_IMAGES) {
            (data as? ImagesResponse)?.profiles?.let {
                adapter.updateDataList(it)
            }
        } else if (id == Constants.GET_PERSON_DETAIL) {
            (data as? Person)?.let { setupUi(it) }
        }
    }

    override fun preLoading(id: Int) {
        super.preLoading(id)
        if (id == Constants.GET_PERSON_DETAIL)
            progressBar.setVisibilityState(View.VISIBLE)
    }

    override fun onComplete(id: Int) {
        super.onComplete(id)
        if (id == Constants.GET_PERSON_DETAIL)
            progressBar.setVisibilityState(View.GONE)
    }

    override fun retry(vararg id: Int) {
        super.retry(*id)
        id.forEach {
            when (it) {
                Constants.GET_PERSON_DETAIL -> viewModel.loadPersonDetail(person?.id ?: 0)
                Constants.GET_PERSON_IMAGES -> viewModel.loadPersonImages(person?.id ?: 0)
            }
        }
    }
}