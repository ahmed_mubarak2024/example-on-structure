package com.mubarak.famousPeopleApp.views.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mubarak.famousPeopleApp.R
import com.mubarak.famousPeopleApp.model.Person
import com.mubarak.famousPeopleApp.views.adapter.parent.BaseAdapter
import com.mubarak.famousPeopleApp.views.adapter.parent.BaseViewHolder
import com.mubarak.famousPeopleApp.views.adapter.viewholder.PersonViewHolder
import io.reactivex.disposables.CompositeDisposable

/**
 * [RecyclerView.Adapter] that can display a [DummyItem].
 * TODO: Replace the implementation with code for your data type.
 */
class PersonListingAdapter(bag: CompositeDisposable) :
    BaseAdapter<Person>(bag) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Person> {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_user, parent, false)
        return PersonViewHolder(view, bag).addToListener()
    }


    override fun getItemId(position: Int): Long {
        return getItem(position).primaryKey.toLong()
    }



}