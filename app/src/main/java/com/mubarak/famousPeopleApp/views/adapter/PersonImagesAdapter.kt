package com.mubarak.famousPeopleApp.views.adapter

import android.view.ViewGroup
import com.mubarak.famousPeopleApp.R
import com.mubarak.famousPeopleApp.model.Profile
import com.mubarak.famousPeopleApp.util.layoutInflater
import com.mubarak.famousPeopleApp.views.adapter.parent.BaseAdapter
import com.mubarak.famousPeopleApp.views.adapter.parent.BaseViewHolder
import com.mubarak.famousPeopleApp.views.adapter.viewholder.PersonImageViewHolder
import io.reactivex.disposables.CompositeDisposable

class PersonImagesAdapter(bag: CompositeDisposable) : BaseAdapter<Profile>(bag) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Profile> {
        return PersonImageViewHolder(
            bag,
            parent.layoutInflater().inflate(R.layout.cell_image_person, parent, false)
        ).addToListener()
    }
}