package com.mubarak.famousPeopleApp.views.fragments.personListing

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.mubarak.famousPeopleApp.R
import com.mubarak.famousPeopleApp.model.ClickEvents
import com.mubarak.famousPeopleApp.model.Person
import com.mubarak.famousPeopleApp.util.*
import com.mubarak.famousPeopleApp.viewmodel.PersonListingViewModel
import com.mubarak.famousPeopleApp.views.adapter.PersonListingAdapter
import com.mubarak.famousPeopleApp.views.parents.BasePagingFragment
import kotlinx.android.synthetic.main.fragment_person_listing.*
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import java.util.concurrent.TimeUnit

/**
 * A fragment representing a list of Items.
 */
class PersonListingFragment : BasePagingFragment<Person>() {
    companion object {
        private const val KEY = "type"
        const val TYPE_POPULAR = "Popular"
        fun newInstance(type: String) =
            PersonListingFragment().apply {
                arguments = Bundle().apply {
                    putString(KEY, type)
                }
            }
    }

    val viewModel by viewModel(PersonListingViewModel::class) { parametersOf(bag) }
    override val adapter by lazy {
        PersonListingAdapter(bag)
    }
    val type by lazy {
        arguments?.getString(KEY) ?: TYPE_POPULAR
    }
    val typeListing by lazy {
        when (type) {
            TYPE_POPULAR -> Constants.GET_POPULAR_ID
            else -> throw IllegalArgumentException("type should be set")
        }
    }
    override val identifier: Int
        get() = typeListing

    override val refreshLayout: SwipeRefreshLayout?
        get() = srl_persons_list


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_person_listing, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rv_persons.adapter = adapter
        rv_persons.layoutManager = GridLayoutManager(mContext, 2)

        viewModel.addListener()
        adapter.clicks
            .filter { it.id != ClickEvents.LOAD_NEXT_PAGE }
            .throttleLatest(700, TimeUnit.MILLISECONDS)
            .observeOnly()
            .subscribe {
                when (it.id) {
                    ClickEvents.GO_TO_PERSON_DETAIL -> {
                        println("go to detail")
                        val context = mContext
                        if (it.data != null)
                            context?.let { it1 -> Router.goToPersonDetail(it1, it.data) }
                    }

                }
            }.add(bag)

        loadData()
        pb_person_listing.addToBag(bag)
    }

    override fun loadData() {
        if (isNetworkAvailable(true, typeListing))
            viewModel.loadNextPage(pagNum, typeListing)
    }

    override fun stopRequest() {
        viewModel.restartRequestChain()
    }

    override fun preLoading(id: Int) {
        super.preLoading(id)
        println("preLoading")
        pb_person_listing.setVisibilityState(View.VISIBLE)
    }

    override fun onComplete(id: Int) {
        super.onComplete(id)
        println("onComplete")
        pb_person_listing.setVisibilityState(View.GONE)

    }


    override fun retry(vararg id: Int) {
        super.retry(*id)
        loadData()
    }

    override fun <G> onSuccess(data: G, id: Int) {
        super.onSuccess(data, id)
        if (adapter.itemCount == 20)
            rv_persons.scrollToPosition(1)
    }
}