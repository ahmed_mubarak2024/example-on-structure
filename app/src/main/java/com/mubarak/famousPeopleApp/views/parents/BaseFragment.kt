package com.mubarak.famousPeopleApp.views.parents

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.mubarak.famousPeopleApp.R
import com.mubarak.famousPeopleApp.model.DataEvent
import com.mubarak.famousPeopleApp.util.add
import com.mubarak.famousPeopleApp.util.observeOnly
import com.mubarak.famousPeopleApp.viewmodel.parent.BaseViewModel
import io.reactivex.disposables.CompositeDisposable
import retrofit2.HttpException
import java.io.IOException


open class BaseFragment : Fragment() {
    val bag: CompositeDisposable = CompositeDisposable()
    var mContext: Context? = null
    var mView: View? = null
    val snackBar by lazy {
        mView?.let {
            Snackbar.make(it, R.string.bad_network, BaseTransientBottomBar.LENGTH_INDEFINITE)
                .setAction(R.string.retry_call) {}.addCallback(object : Snackbar.Callback() {
                    override fun onShown(sb: Snackbar?) {
                        super.onShown(sb)
                        sb?.view?.findViewById<View>(R.id.snackbar_action)?.setOnClickListener {
                            if (isNetworkAvailable())
                                retryAllErrors()
                            else {
                                showErrorSnakeBar()
                                showServerError("Check you Internet Connection")
                            }
                        }
                    }
                })
        }
    }

    private fun retryAllErrors() {
        retry(*errorRequests.toIntArray())
    }

    private val errorRequests: MutableSet<Int> = mutableSetOf()
    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mView = view
        (mContext as? BaseActivity)?.networkListener
            ?.distinctUntilChanged()
            ?.subscribe {
                if (it && errorRequests.isNotEmpty())
                    retryAllErrors()

            }
            ?.add(bag)
    }

    override fun onDetach() {
        super.onDetach()
        mContext = null
    }

    override fun getView(): View? {
        return mView
    }

    override fun getContext(): Context? {
        return mContext
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mView = null
    }

    override fun onDestroy() {
        super.onDestroy()
        bag.clear()
    }

    @CallSuper
    open fun <T> onSuccess(data: T, id: Int) {
        errorRequests.remove(id)
        if (errorRequests.isEmpty()) {
            snackBar?.dismiss()
            snackBar?.setText(R.string.bad_network)
        }
    }

    open fun onFail(throwable: Throwable, id: Int) {
        if (throwable is IOException) {
            if (id != 0)
                errorRequests.add(id)
            showErrorSnakeBar()
        } else {
            showServerError((throwable as? HttpException)?.message ?: throwable.message ?: "")

        }
    }

    open fun onComplete(id: Int) {}
    open fun preLoading(id: Int) {}

    fun BaseViewModel.addListener() {
        this.dataPublisher
            .observeOnly()
            .subscribe {
                when (it.event) {
                    DataEvent.STARTED -> preLoading(it.identifier)
                    DataEvent.ENDED -> onComplete(it.identifier)
                    DataEvent.SUCCESS -> onSuccess(it.data, it.identifier)
                    DataEvent.ERROR -> it.error?.let { it1 -> onFail(it1, it.identifier) }
                }
            }.add(bag)
    }

    fun showErrorSnakeBar() {
        if (snackBar?.isShown == false) {
            snackBar?.show()
        }
    }

    fun showServerError(msg: String) {
        mView?.let {
            val errorStateOn = snackBar?.isShown ?: false
            if (errorStateOn)
                snackBar?.setText(msg)
            else
                Snackbar.make(it, msg, BaseTransientBottomBar.LENGTH_LONG).show()
        }
    }

    open fun retry(vararg id: Int) {}

    protected fun isNetworkAvailable(showShowError: Boolean = false, vararg id: Int): Boolean {
        val connected = (mContext as? BaseActivity)?.checkNetworkConnectivity() ?: false
        if (!connected && showShowError) {
            id.forEach {
                onFail(IOException(), it)
            }
            id.forEach {
                onComplete(it)
            }
        }
        return connected
    }

    protected fun checkErrorState(id: Int) = errorRequests.contains(id)
}