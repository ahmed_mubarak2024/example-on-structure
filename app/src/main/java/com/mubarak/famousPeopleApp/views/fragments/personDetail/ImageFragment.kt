package com.mubarak.famousPeopleApp.views.fragments.personDetail

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.mubarak.famousPeopleApp.R
import com.mubarak.famousPeopleApp.model.Profile
import com.mubarak.famousPeopleApp.util.fromJson
import com.mubarak.famousPeopleApp.util.getGlideBaseOptions
import com.mubarak.famousPeopleApp.util.getImagePath
import com.mubarak.famousPeopleApp.util.toGsonString
import com.mubarak.famousPeopleApp.views.parents.BaseFragment
import kotlinx.android.synthetic.main.fragment_image.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException
import java.io.OutputStream
import java.util.*

/**
 * An example full-screen fragment that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class ImageFragment : BaseFragment() {
    companion object {

        private const val KEY = "KEY"
        fun newInstance(profile: Profile) =
            ImageFragment().apply {
                arguments = Bundle().apply {
                    putString(KEY, profile.toGsonString())
                }
            }
    }

    private val saveFileCode: Int = 11



    private val showPart2Runnable = Runnable {
        // Delayed display of UI elements
        fullscreenContentControls?.visibility = View.VISIBLE
    }
    private var visible: Boolean = false


    private lateinit var fullscreenContent: ImageView
    private var fullscreenContentControls: View? = null
    val profile by lazy {
        arguments?.getString(KEY)?.fromJson<Profile>()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_image, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        visible = true

        fullscreenContent = view.findViewById(R.id.iv_person_image)
        fullscreenContentControls = view.findViewById(R.id.fullscreen_content_controls)
        // Set up the user interaction to manually show or hide the system UI.


        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        bt_save_image.setOnClickListener {
            requestUri()
        }
        Glide.with(view)
            .load(profile?.filePath?.getImagePath())
            .apply(getGlideBaseOptions())
            .into(fullscreenContent)
    }

    private fun requestUri() {
        val intent = Intent(Intent.ACTION_CREATE_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.setType("image/*")
        intent.putExtra(Intent.EXTRA_TITLE, "IMG_" + Date().time.toString() + ".png")
        startActivityForResult(intent, saveFileCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == saveFileCode && resultCode == Activity.RESULT_OK) {
            data?.data?.let {
                mContext?.getContentResolver()?.openOutputStream(it)
                    ?.let { outputStream ->
                        downloadImage(outputStream)
                    }

            }
        }
    }

    private fun downloadImage(outputStream: OutputStream) {

        Glide.with(mContext!!)
            .asBitmap()
            .load(profile?.filePath?.getImagePath())
            .into(object : CustomTarget<Bitmap>() {
                override fun onLoadCleared(placeholder: Drawable?) {
                }

                override fun onResourceReady(
                    resource: Bitmap,
                    transition: Transition<in Bitmap>?
                ) {
                    GlobalScope.launch(Dispatchers.IO) {
                        saveImageToFile(resource, outputStream)
                    }

                }


            })


    }


    fun showToast(msg: String) {
        GlobalScope.launch(Dispatchers.Main) {
            Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show()
        }
    }

    private fun saveImageToFile(resource: Bitmap, outputStream: OutputStream) {

        try {


            outputStream.use { out ->
                resource.compress(
                    Bitmap.CompressFormat.PNG,
                    100,
                    out
                ) // bmp is your Bitmap instance
            }
            showToast("image saved")


        } catch (e: IOException) {
            e.printStackTrace()
            showToast("image could not be saved ")
        }
    }




}