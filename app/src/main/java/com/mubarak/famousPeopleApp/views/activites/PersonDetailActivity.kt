package com.mubarak.famousPeopleApp.views.activites

import android.os.Bundle
import com.mubarak.famousPeopleApp.R
import com.mubarak.famousPeopleApp.views.fragments.personDetail.PersonDetailFragment
import com.mubarak.famousPeopleApp.views.parents.BaseActivity
import com.mubarak.famousPeopleApp.views.parents.BaseFragment

class PersonDetailActivity : BaseActivity() {
    companion object {
        const val KEY_PERSON = "key_person"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_person_detail)
        //println("PersonDetailActivity")
        addFragment(PersonDetailFragment.newInstance(intent.extras))
    }

    override fun addFragment(newInstance: BaseFragment, addToBackStack: Boolean) {
        supportFragmentManager.beginTransaction()
            .add(R.id.container, newInstance).apply {
                if (addToBackStack)
                    addToBackStack(null)
            }.commitAllowingStateLoss()
    }
}