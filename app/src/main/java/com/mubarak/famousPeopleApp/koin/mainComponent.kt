package com.mubarak.famousPeopleApp.koin

import com.mubarak.famousPeopleApp.dataSources.PeopleDataSourceApi
import com.mubarak.famousPeopleApp.repos.PersonsRepo
import com.mubarak.famousPeopleApp.retrofit.RetroFItTester
import com.mubarak.famousPeopleApp.retrofit.RetrofitCreator
import com.mubarak.famousPeopleApp.util.Constants
import com.mubarak.famousPeopleApp.viewmodel.PersonDetailViewModel
import com.mubarak.famousPeopleApp.viewmodel.PersonListingViewModel
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import org.koin.java.KoinJavaComponent
import java.util.concurrent.TimeUnit

/**
 * should have all the Android Models
 * */
val androidModule = module {

}

/**
 * should have all the Retrofit models and
 * have the the main [retrofit2.Retrofit]
 * have the OkHttpClient
 * */
val retroFitModule = module {
    single { RetrofitCreator.getInstance() as RetroFItTester }
    single {
        get<RetroFItTester>().getAuthorizedOkHttpClient()
            .readTimeout(10000, TimeUnit.MILLISECONDS)
            .connectTimeout(10000, TimeUnit.MILLISECONDS)
            .build()
    }

}

/**
 * should have all the retroFit Apis
 * */
val apiModule = module {
    single { get<RetroFItTester>().getPopularPeopleApi() }

}

/**
 * should have all the dataSources
 * it is using the [org.koin.core.module.Module.factory] methods to generate
 * new instance for each View
 * they all take bag to create the [com.mubarak.famousPeopleApp.dataSources.BaseDataSource]
 * */
val dataSource = module {
    factory { (bag: CompositeDisposable) -> PeopleDataSourceApi(bag) }

}

/**
 * should have all the Repos
 * it is using the [org.koin.core.module.Module.factory] methods to generate
 * new instance for each View
 * they all take bag to create the [com.mubarak.famousPeopleApp.repos.BaseRepo]
 * */
val apiRepoModule = module {
    factory { (bag: CompositeDisposable) -> PersonsRepo(bag) }
}

/**
 * should have all the viewModels
 * they all take bag to create the internal instances Repos
 * */
val viewModels = module {
    viewModel { (bag: CompositeDisposable) -> PersonListingViewModel(bag) }
    viewModel { (bag: CompositeDisposable) -> PersonDetailViewModel(bag) }
}

/**
 * should contain all the Rx models
 * */
val rxModule = module {
    // provided components
    /**
     * [SchedulerProvider] have all the [Scheduler] used in the app
     * */
    single { ApplicationSchedulerProvider() as SchedulerProvider }

}

val koinModule =
    viewModels + apiModule + dataSource + apiRepoModule + retroFitModule + rxModule

fun isTestEnvironment() =
    !KoinJavaComponent.get(Boolean::class.java, named(Constants.IS_TESTING))