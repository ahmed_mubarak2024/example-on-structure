package com.mubarak.famousPeopleApp.koin

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * [SchedulerProvider] have all the [Scheduler] used in the app
 * */
interface SchedulerProvider {
    fun io(): Scheduler
    fun ui(): Scheduler
    fun computation(): Scheduler
    fun single(): Scheduler
}

/**
 * implement [SchedulerProvider] to use in the running environment
 * */
class ApplicationSchedulerProvider : SchedulerProvider {
    override fun io() = Schedulers.io()

    override fun ui() = AndroidSchedulers.mainThread()

    override fun computation() = Schedulers.computation()
    override fun single(): Scheduler = Schedulers.single()
}

