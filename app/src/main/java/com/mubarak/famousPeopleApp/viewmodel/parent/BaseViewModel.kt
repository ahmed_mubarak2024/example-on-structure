package com.mubarak.famousPeopleApp.viewmodel.parent

import androidx.lifecycle.ViewModel
import com.mubarak.famousPeopleApp.model.DataEvent
import com.mubarak.famousPeopleApp.repos.BaseRepo
import io.reactivex.subjects.PublishSubject
import org.koin.core.KoinComponent

/**
 *
 * */
abstract class BaseViewModel : ViewModel(), KoinComponent {
    val dataPublisher: PublishSubject<DataEvent<*>> by
    lazy { PublishSubject.create<DataEvent<*>>() }

    fun BaseRepo.initDataPublisher() {
        this.dataPublisher
            .subscribe(this@BaseViewModel.dataPublisher)

    }
}