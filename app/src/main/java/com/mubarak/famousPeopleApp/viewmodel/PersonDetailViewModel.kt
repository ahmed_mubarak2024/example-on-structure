package com.mubarak.famousPeopleApp.viewmodel

import com.mubarak.famousPeopleApp.repos.PersonsRepo
import com.mubarak.famousPeopleApp.viewmodel.parent.BaseViewModel
import io.reactivex.disposables.CompositeDisposable
import org.koin.core.inject
import org.koin.core.parameter.parametersOf

class PersonDetailViewModel(bag: CompositeDisposable) : BaseViewModel() {
    private val personsRepo by inject<PersonsRepo> { parametersOf(bag) }

    init {
        personsRepo.initDataPublisher()
    }

    fun loadPersonDetail(personId: Int) = personsRepo.loadPersonDetail(personId)
    fun loadPersonImages(personId: Int) = personsRepo.loadPersonImages(personId)
}