package com.mubarak.famousPeopleApp.dataSources

import com.mubarak.famousPeopleApp.model.DataEvent
import com.mubarak.famousPeopleApp.util.add
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import org.koin.core.KoinComponent

/**
 * here we have the base logic for all the data sources
 * @param bag is used to add the subscription for all the reactive call using RXjava
 * @property dataPublisher used to relay all the event to the owner of the instance
 * */
open class BaseDataSource(protected val bag: CompositeDisposable) : KoinComponent {
    val dataPublisher: PublishSubject<DataEvent<*>> by
    lazy { PublishSubject.create<DataEvent<*>>() }

    /**
     * @param id identifier to differ between calls and help the user of the repo to work know
     * what call been made
     * it handles the success state which is [DataEvent.SUCCESS] always mapped in the child
     * and the [DataEvent.STARTED], [DataEvent.ENDED],[DataEvent.ERROR]
     * */
    fun Single<DataEvent<*>>.subscribeOnPublisher(id: Int) =
        this.doOnSubscribe { dataPublisher.onNext(DataEvent.onStart(id)) }
            .doAfterTerminate { dataPublisher.onNext(DataEvent.onEnd(id)) }
            .subscribe({
                dataPublisher.onNext(it)
            }, {
                dataPublisher.onNext(DataEvent.onError(id, it))
            }).add(bag)
}