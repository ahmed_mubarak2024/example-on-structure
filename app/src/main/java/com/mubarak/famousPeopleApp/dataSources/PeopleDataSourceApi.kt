package com.mubarak.famousPeopleApp.dataSources

import com.mubarak.famousPeopleApp.dataSources.PeopleDataSourceApi.Companion.GET_PERSON_DETAIL
import com.mubarak.famousPeopleApp.dataSources.PeopleDataSourceApi.Companion.GET_PERSON_IMAGES
import com.mubarak.famousPeopleApp.dataSources.PeopleDataSourceApi.Companion.GET_POPULAR_ID
import com.mubarak.famousPeopleApp.model.DataEvent
import com.mubarak.famousPeopleApp.retrofit.apis.PeopleApi
import com.mubarak.famousPeopleApp.util.Constants
import com.mubarak.famousPeopleApp.util.subscribeOnly
import io.reactivex.disposables.CompositeDisposable
import org.koin.core.inject
import retrofit2.http.Query


/**
 * @property GET_PERSON_DETAIL
 * @property GET_PERSON_IMAGES
 * @property GET_POPULAR_ID they are the identifiers for all the calls form this data source
 * they excite in the [Constants] file but i add them here for easy reading
 * @property api the retrofit instance for [PeopleApi]
 * */
class PeopleDataSourceApi(bag: CompositeDisposable) : BaseDataSource(bag) {
    companion object {
        const val GET_POPULAR_ID = Constants.GET_POPULAR_ID
        const val GET_PERSON_DETAIL = Constants.GET_PERSON_DETAIL
        const val GET_PERSON_IMAGES = Constants.GET_PERSON_IMAGES
    }

    private val api: PeopleApi by inject()

    /**
     * call for [Constants.PERSON_POPULAR]
     * @param page [Query] for the api
     * notice the map function to map the result data to [DataEvent]
     * */
    private fun getPopular(page: Int) {
        api.getPopular(page)
            .subscribeOnly()
            .map { results ->
                DataEvent.onSuccess(GET_POPULAR_ID, results.results) as DataEvent<*>
            }
            .subscribeOnPublisher(GET_POPULAR_ID)
    }


    /**
     * call for [Constants.PERSON_DETAIL]
     * @param personId [retrofit2.http.Path] for the api
     * notice the map function to map the result data to [DataEvent]
     * */
    private fun getPersonDetail(personId: Int) {
        api.getPersonDetail(personId)
            .subscribeOnly()
            .map { result ->
                DataEvent.onSuccess(GET_PERSON_DETAIL, result) as DataEvent<*>
            }
            .subscribeOnPublisher(GET_PERSON_DETAIL)
    }


    /**
     * call for [Constants.PERSON_IMAGES]
     * @param personId [retrofit2.http.Path] for the api
     * notice the map function to map the result data to [DataEvent]
     * */
    private fun getPersonImages(personId: Int) {
        api.getPersonImages(personId)
            .subscribeOnly()
            .map { result ->
                DataEvent.onSuccess(GET_PERSON_IMAGES, result) as DataEvent<*>
            }
            .subscribeOnPublisher(GET_PERSON_IMAGES)
    }

    /**
     * call for [getPopular]
     * @param page [Query] for the api
     * run the api call in the data source
     * */
    fun loadPopular(page: Int) = getPopular(page)

    /**
     * call for [getPersonDetail]
     * @param personId [Query] for the api
     * run the api call in the data source
     * */
    fun loadPersonDetail(personId: Int) = getPersonDetail(personId)

    /**
     * call for [getPersonImages]
     * @param personId [Query] for the api
     * run the api call in the data source
     * */
    fun loadPersonImages(personId: Int) = getPersonImages(personId)
}