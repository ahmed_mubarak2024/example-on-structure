package com.mubarak.famousPeopleApp

import androidx.annotation.CallSuper
import com.mubarak.famousPeopleApp.util.ObjectParser
import com.mubarak.famousPeopleApp.util.assertNotNull
import com.mubarak.famousPeopleApp.util.testModules
import io.reactivex.observers.TestObserver
import org.junit.After
import org.junit.Before
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.MockitoAnnotations

open class BaseTest : KoinTest {

    val objectParser: ObjectParser by inject()

    @Before
    @CallSuper
    open fun before() {
        MockitoAnnotations.initMocks(this)
        stopKoin()
        startKoin {
            this.modules(testModules)

//            this.logger(AndroidLogger(Level.DEBUG))
        }
    }

    @After
    @CallSuper
    open fun after() {
        stopKoin()
    }


    fun <T> TestObserver<T>.normalTest() =
        this.assertOf {
            if (it.errorCount() > 0)
                throw it.errors()[0]
        }
            .assertNotNull({ it })
            .assertNoErrors()
            .assertComplete()


}