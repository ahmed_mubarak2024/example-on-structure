package com.mubarak.famousPeopleApp.util

import io.reactivex.observers.TestObserver
import org.junit.Assert
import java.io.File

interface Value<T> {
    fun getValue(data: T): Any
}

fun <T, G : Any> TestObserver<T>.assertEquals(p1: (T) -> Any?, p2: G, message: String? = null) =
    this.assertOf { testObserver ->

        testObserver.values().forEach {
            Assert.assertEquals(message, p2, p1.invoke(it))
        }

    }

fun <T> TestObserver<T>.assertNotNull(p1: (T) -> Any?, message: String? = null) =
    this.assertOf {
        if (it.valueCount() > 0) {
            it.values().forEach {
                message?.let { message -> Assert.assertNotNull(message, p1.invoke(it)) }
                    ?: Assert.assertNotNull(p1.invoke(it))
            }
        } else {
            println("${it.valueCount()} ${it.isTimeout}")
            throw Throwable(message ?: "No Values present")
        }
    }

fun <T : List<R>, R> TestObserver<T>.assertNotNullArray(p1: (R) -> Any?, message: String? = null) =
    this.assertOf {
        if (it.valueCount() > 0) {
            it.values().forEach {
                it.forEach {
                    message?.let { message -> Assert.assertNotNull(message, p1.invoke(it)) }
                        ?: Assert.assertNotNull(p1.invoke(it))
                }

            }
        } else {
            println("${it.valueCount()} ${it.isTimeout}")
            throw Throwable(message ?: "No Values present")
        }
    }

/**
 * run on any java instance class get the file from the resources in gradle build system
 * @param fileName the full file name of the file
 * */
fun Any.readResourceString(fileName: String) =
    try {
        if (!fileName.isEmpty()) {
            val text = if (fileName.contains("\\")) {
                val fileNames =
                    if (fileName.contains("\\")) fileName.split("\\") else fileName.split("/")
                val parent = fileNames.get(0)
                val resources = javaClass.classLoader?.getResource(parent)
                val path = resources?.path
                val resource = File(path)
                val openStream = File(resource, fileNames[1].toString())
                openStream.inputStream().bufferedReader().use {
                    val readText = it.readText()
                    readText
                }
            } else
                javaClass.classLoader?.getResourceAsStream(fileName)?.bufferedReader()
                    ?.use { it.readText() }
            //println(text)
            text
        }
        //println(text)
        else "{}"
    } catch (e: Exception) {
        ""
    }
