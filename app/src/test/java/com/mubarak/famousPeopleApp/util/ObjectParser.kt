package com.mubarak.famousPeopleApp.util


import com.google.gson.Gson
import io.reactivex.Single
import org.koin.core.KoinComponent
import java.lang.reflect.Type

class ObjectParser(private val gson: Gson) : KoinComponent {


    fun <T> parseObject(fileName: String, classOfT: Class<T>): Single<T> {
        return Single.just(gson.fromJson(readResourceString(fileName), classOfT))
    }


    fun <T> parseObject(fileName: String, type: Type): Single<T> {
        return Single.just(gson.fromJson(readResourceString(fileName), type) as T)
    }


}