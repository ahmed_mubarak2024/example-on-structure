package com.mubarak.famousPeopleApp.util


import com.google.gson.Gson
import com.mubarak.famousPeopleApp.koin.SchedulerProvider
import com.mubarak.famousPeopleApp.koin.koinModule
import org.koin.dsl.module


val testRxModule = module {
    single(override = true) { TestSchedulerProvider() as SchedulerProvider }

}
val objectReader = module {
    single { ObjectParser(Gson()) }
}


val testModules = koinModule + testRxModule + objectReader
