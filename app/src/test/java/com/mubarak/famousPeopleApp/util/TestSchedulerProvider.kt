package com.mubarak.famousPeopleApp.util

import com.mubarak.famousPeopleApp.koin.SchedulerProvider
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

class TestSchedulerProvider : SchedulerProvider {
    override fun io() = Schedulers.trampoline()

    override fun ui() = Schedulers.trampoline()

    override fun computation() = Schedulers.trampoline()
    override fun single(): Scheduler = Schedulers.trampoline()
}