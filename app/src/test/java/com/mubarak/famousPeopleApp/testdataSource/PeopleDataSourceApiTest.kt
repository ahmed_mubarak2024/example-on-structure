package com.mubarak.famousPeopleApp.testdataSource

import com.mubarak.famousPeopleApp.BaseTest
import com.mubarak.famousPeopleApp.mockedApi.PersonApiMocked
import com.mubarak.famousPeopleApp.model.DataEvent
import com.mubarak.famousPeopleApp.repos.PersonsRepo
import com.mubarak.famousPeopleApp.retrofit.apis.PeopleApi
import com.mubarak.famousPeopleApp.util.Constants
import io.reactivex.disposables.CompositeDisposable
import org.junit.Test
import org.koin.core.context.loadKoinModules
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module
import org.koin.test.inject
import java.util.concurrent.TimeUnit

class PeopleDataSourceApiTest : BaseTest() {
    private val personRepo by inject<PersonsRepo> { parametersOf(CompositeDisposable()) }
    override fun before() {
        super.before()
        loadKoinModules(module {
            single(override = true) { PersonApiMocked() as PeopleApi }
        })
    }

    /*each request should have three states start success ended*/
    @Test
    fun testSuccessCalls() {
        val test = personRepo.dataPublisher.test()
        test
            .awaitTerminalEvent(2, TimeUnit.SECONDS)
        personRepo.loadPersonDetail(1)
        personRepo.loadPersonImages(1)
        personRepo.loadPage(1, Constants.GET_POPULAR_ID)

        test.assertValueCount(9)

    }

    @Test
    fun testCallStates() {
        fun testCall(x: () -> Unit) {
            val test = personRepo.dataPublisher.test()
            test
                .awaitTerminalEvent(2, TimeUnit.SECONDS)
            x.invoke()
            test.assertValueAt(0) { it.event == DataEvent.STARTED }
            test.assertValueAt(1) { it.event == DataEvent.SUCCESS }
            test.assertValueAt(2) { it.event == DataEvent.ENDED }
        }

        testCall { personRepo.loadPersonDetail(1) }


        testCall { personRepo.loadPersonImages(1) }


        testCall { personRepo.loadPage(1, Constants.GET_POPULAR_ID) }
    }

    @Test
    fun testOnlyLoadPageConstantWork() {
        val test = personRepo.dataPublisher.test()
        test
            .awaitTerminalEvent(2, TimeUnit.SECONDS)
        personRepo.loadPage(1, 0)
        test.assertValueCount(0)
    }

    @Test
    fun testErrorState() {
        loadKoinModules(module {
            single(override = true) { PersonApiMocked(false) as PeopleApi }
        })
        val test = personRepo.dataPublisher.test()
        test
            .awaitTerminalEvent(2, TimeUnit.SECONDS)
        personRepo.loadPersonImages(1)
        test.assertValueAt(0) { it.event == DataEvent.STARTED }
        test.assertValueAt(1) { it.event == DataEvent.ERROR }
        test.assertValueAt(2) { it.event == DataEvent.ENDED }
    }
}