package com.mubarak.famousPeopleApp.mockedApi

import com.google.gson.reflect.TypeToken
import com.mubarak.famousPeopleApp.mockedApi.parent.MockedBase
import com.mubarak.famousPeopleApp.model.ImagesResponse
import com.mubarak.famousPeopleApp.model.Person
import com.mubarak.famousPeopleApp.model.Result
import com.mubarak.famousPeopleApp.retrofit.apis.PeopleApi
import io.reactivex.Single

class PersonApiMocked(val returnData: Boolean = true) : PeopleApi, MockedBase() {
    override fun getPopular(page: Int): Single<Result<Person>> {
        val type = object : TypeToken<Result<Person>>() {}.type
        return if (returnData)
            Single.just(getData("list_popular", type))
        else Single.error(Exception())
    }

    override fun getPersonDetail(personId: Int): Single<Person> {
        return if (returnData)
            Single.just(getData("person_detail", Person::class.java))
        else Single.error(Exception())
    }

    override fun getPersonImages(personId: Int): Single<ImagesResponse> {
        return if (returnData)
            Single.just(getData("list_images", ImagesResponse::class.java))
        else Single.error(Exception())

    }
}