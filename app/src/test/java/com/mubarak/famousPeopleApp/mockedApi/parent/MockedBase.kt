package com.mubarak.famousPeopleApp.mockedApi.parent

import com.google.gson.reflect.TypeToken
import com.mubarak.famousPeopleApp.BaseTest
import java.lang.reflect.Type

open class MockedBase : BaseTest() {
    fun <T> getData(fileName: String, clazz: Class<T>) =
        objectParser.parseObject(fullFileName(fileName), clazz).blockingGet()

    fun <T> getData(fileName: String, type: Type) =
        objectParser.parseObject<T>(fullFileName(fileName), type).blockingGet()

    inline fun <reified T> getType() = object : TypeToken<T>() {}.type
    open fun fullFileName(fileName: String): String =
        "${javaClass.name.substringAfterLast(".")}\\$fileName"

}