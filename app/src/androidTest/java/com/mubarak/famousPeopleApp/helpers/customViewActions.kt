package com.mubarak.famousPeopleApp.helpers

import android.view.View
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import org.hamcrest.Matcher
import org.hamcrest.Matchers

fun clickChildViewWithId(id: Int): ViewAction {
    return object : ViewAction {
        override fun getConstraints(): Matcher<View>? {
            return Matchers.allOf()
        }

        override fun getDescription(): String {
            return "Click on ProfileRequest child view with specified id."
        }

        override fun perform(uiController: UiController?, view: View) {
            val v: View? = view.findViewById(id)
            v?.callOnClick()

        }
    }
}