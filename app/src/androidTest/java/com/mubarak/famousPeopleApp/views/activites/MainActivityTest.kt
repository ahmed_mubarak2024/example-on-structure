package com.mubarak.famousPeopleApp.views.activites


import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.NestedScrollView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.runner.AndroidJUnit4
import com.mubarak.famousPeopleApp.R
import com.mubarak.famousPeopleApp.parent.BaseHomeTest
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.instanceOf
import org.hamcrest.core.IsInstanceOf
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest : BaseHomeTest() {


    @Test
    fun checkClickOnItemOpenItemDetail() {
        Thread.sleep(3000)
        clickOnItem(6)
        Thread.sleep(3000)

        val recyclerView = onView(
            allOf(
                withId(R.id.rv_images),
                withParent(
                    allOf(
                        instanceOf(ConstraintLayout::class.java),
                        withParent(IsInstanceOf.instanceOf(NestedScrollView::class.java))
                    )
                ),
                isDisplayed()
            )
        )
        recyclerView.check(matches(isDisplayed()))

    }


}
