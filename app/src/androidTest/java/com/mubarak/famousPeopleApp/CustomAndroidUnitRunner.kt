package com.mubarak.famousPeopleApp

import androidx.test.runner.AndroidJUnitRunner
import com.squareup.rx2.idler.Rx2Idler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins


class CustomAndroidUnitRunner : AndroidJUnitRunner() {
    override fun onStart() {

        RxJavaPlugins.setInitIoSchedulerHandler(
            Rx2Idler.create("RxJava 2.x Io Scheduler")
        )
        super.onStart()
        RxJavaPlugins.setInitIoSchedulerHandler(
            Rx2Idler.create("RxJava 2.x Io Scheduler")
        )
        RxJavaPlugins.setInitComputationSchedulerHandler(
            Rx2Idler.create("RxJava 2.x Computation Scheduler")
        )

        RxJavaPlugins.setInitNewThreadSchedulerHandler(
            Rx2Idler.create("RxJava 2.x NewThreadS Scheduler")
        )
        RxJavaPlugins.setInitSingleSchedulerHandler(
            Rx2Idler.create("RxJava 2.x Single Scheduler")
        )
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(Rx2Idler.create("Rxjava 2.x MainThreads Scheduler"))

    }
}