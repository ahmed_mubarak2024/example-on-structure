package com.mubarak.famousPeopleApp.parent

import androidx.test.espresso.Espresso
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.rule.ActivityTestRule
import com.mubarak.famousPeopleApp.R
import com.mubarak.famousPeopleApp.helpers.clickChildViewWithId
import com.mubarak.famousPeopleApp.views.activites.MainActivity
import com.mubarak.famousPeopleApp.views.adapter.parent.BaseViewHolder
import org.junit.After

abstract class BaseHomeTest : BaseTestHelper() {

    override val mActivityTestRule = ActivityTestRule(MainActivity::class.java)


    override fun before() {
        super.before()
        mActivityTestRule.launchActivity(null)
    }

    fun clickOnItem(index: Int) {
        Espresso.onView(ViewMatchers.withId(R.id.rv_persons))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<BaseViewHolder<*>>(
                    index,
                    clickChildViewWithId(R.id.iv_people_imge)
                )
            )

    }

    @After
    fun after() {
        mActivityTestRule.finishActivity()
    }
}