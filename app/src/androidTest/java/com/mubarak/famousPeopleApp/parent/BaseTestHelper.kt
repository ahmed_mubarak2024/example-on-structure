package com.mubarak.famousPeopleApp.parent

import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.UiDevice
import com.jakewharton.espresso.OkHttp3IdlingResource
import com.mubarak.famousPeopleApp.views.parents.BaseActivity
import okhttp3.OkHttpClient
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher
import org.junit.Before
import org.koin.core.KoinComponent
import org.koin.core.inject

open class BaseTestHelper : KoinComponent {
    val okHttpClient by inject<OkHttpClient>()
    protected val resource by lazy { OkHttp3IdlingResource.create("OkHttp", okHttpClient) }
    open val mActivityTestRule: ActivityTestRule<out BaseActivity>? = null

    @Before
    @CallSuper
    open fun before() {
        IdlingRegistry.getInstance().register(resource)
        //mActivityTestRule?.launchActivity(null)
    }

    protected fun pressBackNavigation() {
        val mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        mDevice.pressBack()
        Thread.sleep(300)
    }

    fun getContext() =
        InstrumentationRegistry.getInstrumentation().targetContext

    fun ViewInteraction.preformClick() =
        this.check { view, noViewFoundException ->
            view.performClick()
        }

    protected fun ViewInteraction.click() = this.perform(ViewActions.click())
    protected fun ViewInteraction.withText(text: String) =
        this.check(ViewAssertions.matches(ViewMatchers.withText(text)))

    protected fun ViewInteraction.withTextResource(text: Int) =
        this.check(ViewAssertions.matches(ViewMatchers.withText(text)))

    protected fun getString(strResource: Int) = getContext().getString(strResource)
    protected fun childAtPosition(
        parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }

}